import {Article} from './article';
import {HttpClient} from '../scripts/aurelia-http-client.js';


let client = new HttpClient();
let apiUrl = 'http://smirnow.pl/finder/demo/public/api';
let articlesCount = 5;

export class App {
  constructor() {
    this.heading = "Top five posts";
    this.articles = [];
    this.loadData(apiUrl);
  }

  loadData(url) {
    client.get(url)
    .then(response => {
      this.populateArticles(response.content, articlesCount);
  });
  }

  populateArticles(data, count) {
    data.splice(count);

    this.articles = data.map(item => new Article(item))
  }
}

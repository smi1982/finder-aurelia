export class Article {
  constructor({ title, url, thumbnail, description }) {
    this.title = title;
    this.url = url;
    this.thumbnail = thumbnail;
    this.description = description;
  }
}
